;;; convox.el --- Package for interacting with convox (https://convox.com/)

;; Copyright 2018 Peter Debelak <pdebelak@gmail.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; This library helps you run convox commands and interact with their
;; output in GNU Emacs.

;; All commands except convox-login use the currenct convox-app if in
;; a convox buffer or prompts for one of the available apps. Execute a
;; command with C-u prefix to prompt for a convox app regardless.

;; Commands

;; convox-login logs into a rack based on the available racks in your
;; convox-auth-file

;; convox-ps runs convox ps

;; convox-logs runs convox logs

;; convox-exec runs a command on a given box

;; convox-run runs a command on a new box

;; convox-run-detached runs a command detached on a new box

;; convox-apps-info runs the apps info command

;;; Code:
;; throw error if convox not available
(unless (executable-find "convox")
  (error "'convox' not found in path"))

(require 'seq)
(require 'json)

(defgroup convox nil
  "Options for convox plugin"
  :group 'tools)

(defcustom convox-auth-file "~/.convox/auth"
  "Auth file to read to find available convox racks."
  :group 'convox
  :type 'string)

;; cached list of convox apps
(defvar convox-apps-list nil)
;; currently selected app, if present
(defvar convox-app nil)
(make-variable-buffer-local 'convox-app)

(defun convox-reset-apps-list ()
  "Reset the list of convox apps."
  (interactive)
  (setq convox-apps-list
	(split-string
	 (shell-command-to-string "convox apps | tail -n +2 | awk '{ print $1 }'"))))

(defun convox-app-processes (app)
  "Get a list of an app's processes.

APP is name of app to get the processes for."
  (seq-reduce
   (lambda (processes process)
     (if (member process processes)
	 processes
       (cons process processes)))
   (split-string
    (shell-command-to-string (concat "convox ps --app " app " | tail -n +2 | awk '{ print $2 }'")))
  nil))

(defun convox-app-shas (app)
  "Get a list of an app's shas.

APP is name of app to get the processes for."
  (split-string
   (shell-command-to-string (concat "convox ps --app " app " | tail -n +2 | awk '{ print $1 }'"))))

(defun convox-apps-info (&optional force-select-app)
  "Run apps info on app.

FORCE-SELECT-APP will not use current app, even if selected."
  (interactive "P")
  (let ((selected-app (convox-selected-app force-select-app)))
    (let ((output-buffer (convox-buffer "info" selected-app)))
      (shell-command (concat "convox apps info --app " selected-app) output-buffer))))

(defun convox-ps (&optional force-select-app)
  "Ouput convox ps to buffer.

FORCE-SELECT-APP will not use current app, even if selected."
  (interactive "P")
  (let ((selected-app (convox-selected-app force-select-app)))
    (let ((output-buffer (convox-buffer "ps" selected-app)))
      (shell-command (concat "convox ps --app " selected-app) output-buffer))))

(defun convox-env (&optional force-select-app)
  "Output env for app to buffer.

FORCE-SELECT-APP will not use current app, even if selected."
  (interactive "P")
  (let ((selected-app (convox-selected-app force-select-app)))
    (let ((output-buffer (convox-buffer "env" selected-app)))
      (shell-command (concat "convox env --app " selected-app) output-buffer))))

(defun convox-exec (&optional force-select-app)
  "Run command on convox box.

Uses sha at point if possible. FORCE-SELECT-APP will not use
current app, even if selected."
  (interactive "P")
  (let ((point-sha (thing-at-point 'word))
	(selected-app (convox-selected-app force-select-app)))
    (let ((shas (convox-app-shas selected-app)))
      (let ((sha
	     (if (and point-sha
		      (member point-sha shas))
		 point-sha
	       (if (eq (length shas) 1)
		   (car shas)
		 (ido-completing-read "Select sha: " shas))))
	    (command (read-string "Enter command (default bash): " nil 'convox-command-history "bash"))
	    (output-buffer (convox-buffer "exec" selected-app)))
      (async-shell-command
       (concat "convox exec " sha " \"" command "\" --app " selected-app)
       output-buffer)))))

(defun convox-run (&optional force-select-app)
  "Run command in new convox instance.

FORCE-SELECT-APP will not use current app, even if selected."
  (interactive "P")
  (let ((point-process (thing-at-point 'word))
   	(selected-app (convox-selected-app force-select-app)))
    (let ((processes (convox-app-processes selected-app)))
      (let ((process
	     (if (and point-process
		      (member point-process processes))
		 point-process
	       (if (eq (length processes) 1)
		   (car processes)
		 (ido-completing-read "Select process: " processes))))
	    (command (read-string "Enter command (default bash): " nil 'convox-command-history "bash"))
	    (output-buffer (convox-buffer "run" selected-app)))
	(async-shell-command
	 (concat "convox run --app " selected-app " " process " \"" command "\"")
	 output-buffer)))))

(defun convox-run-detached (&optional force-select-app)
    "Run command in new convox instance detached.

FORCE-SELECT-APP will not use current app, even if selected."
  (interactive "P")
  (let ((point-process (thing-at-point 'word))
   	(selected-app (convox-selected-app force-select-app)))
    (let ((processes (convox-app-processes selected-app)))
      (let ((process
	     (if (and point-process
		      (member point-process processes))
		 point-process
	       (if (eq (length processes) 1)
		   (car processes)
		 (ido-completing-read "Select process: " processes))))
	    (command (read-string "Enter command: " nil 'convox-command-history)))
	(shell-command
	 (concat "convox run --detach --app " selected-app " " process " \"" command "\""))))))

(defun convox-login ()
  "Login to convox rack."
  (interactive)
  (let ((racks
	 (seq-reduce
	  (lambda (auth-keys auth) (cons (symbol-name (car auth)) auth-keys))
	  (json-read-file convox-auth-file)
	  nil)))
    (shell-command (concat "convox login " (ido-completing-read "Rack: " racks))))
  (convox-reset-apps-list))

(defun convox-logs (&optional force-select-app)
  "See logs for convox app.

FORCE-SELECT-APP will not use current app, even if selected."
  (interactive "P")
  (let ((selected-app (convox-selected-app force-select-app)))
    (let ((output-buffer (convox-buffer "logs" selected-app)))
      (async-shell-command (concat "convox logs --app " selected-app) output-buffer))))

(defun convox-generated-apps-list ()
  "List of convox apps."
  (unless convox-apps-list
    (convox-reset-apps-list))
  convox-apps-list)

(defun convox-buffer (name selected-app)
  "Switch to convox-enabled buffer.

NAME is name of command and SELECTED-APP is app."
  (let ((output-buffer (concat "*convox " name "<" selected-app ">*")))
    (let ((existing-buffer-window
	   (and
	    (get-buffer output-buffer)
	    (get-buffer-window (get-buffer output-buffer)))))
      (if existing-buffer-window
	  (select-window existing-buffer-window)
	(switch-to-buffer-other-window output-buffer))
      (convox-mode)
      (setq convox-app selected-app)
      output-buffer)))


(defun convox-selected-app (force-select-app)
  "Find or prompt for selected app.

FORCE-SELECT-APP will always prompt."
  (if (or force-select-app (not convox-app))
      (ido-completing-read "Select app: " (convox-generated-apps-list))
    convox-app))

(defvar convox-mode-map nil)

(unless convox-mode-map
  (setq convox-mode-map (make-sparse-keymap))
  (define-key convox-mode-map (kbd "h") 'describe-mode)
  (define-key convox-mode-map (kbd "e") 'convox-exec)
  (define-key convox-mode-map (kbd "r") 'convox-run)
  (define-key convox-mode-map (kbd "d") 'convox-run-detached)
  (define-key convox-mode-map (kbd "i") 'convox-apps-info)
  (define-key convox-mode-map (kbd "l") 'convox-logs)
  (define-key convox-mode-map (kbd "p") 'convox-ps))

(defun convox-mode ()
  "Convox mode is a mode for interacting with convox.

All commands except \\[convox-login] use the current
convox-app if in a convox buffer or prompts for one of the
available apps. Execute a command with prefix to prompt for a
convox app regardless.

Commands

\\[convox-login] logs into a rack based on the available racks in
your convox-auth-file

\\[convox-ps] runs convox ps

\\[convox-logs] runs convox logs

\\[convox-exec] runs a command on a given box defaulting to sha
at point or prompting for sha

\\[convox-run] runs a command on a new box in given process

\\[convox-run-detached] runs a command detached on a new box in a
given process

\\{convox-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (setq buffer-read-only t)
  (use-local-map convox-mode-map)
  (setq mode-name "Convox")
  (setq major-mode 'convox-mode)
  (run-hooks 'text-mode-hook))
(provide 'convox)
;;; convox.el ends here
