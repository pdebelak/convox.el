# convox.el --- Package for interacting with convox (https://convox.com/)

This library helps you run convox commands and interact with their
output in GNU Emacs.

All commands except convox-login use the currenct convox-app if in
a convox buffer or prompts for one of the available apps. Execute a
command with C-u prefix to prompt for a convox app regardless.

## Commands

convox-login logs into a rack based on the available racks in your
convox-auth-file

convox-ps runs convox ps

convox-logs runs convox logs

convox-exec runs a command on a given box

convox-run runs a command on a new box

convox-run-detached runs a command detached on a new box